#!/bin/bash

# Updating repository.
sudo apt-get -y update

# Installing Apache
sudo apt-get -y install apache2

# Installing MySQL and it's dependencies, Also, setting up root password for MySQL as it will prompt to enter the password during installation

sudo debconf-set-selections <<< 'mysql-server-5.5 mysql-server/root_password password rootpass'
sudo debconf-set-selections <<< 'mysql-server-5.5 mysql-server/root_password_again password rootpass'
sudo apt-get -y install mysql-server libapache2-mod-auth-mysql php5-mysql

# Installing PHP and it's dependencies
sudo apt-get -y install php5 libapache2-mod-php5 php5-mcrypt

# Enable mod_rewrite and htaccess files.
sudo a2enmod rewrite
sed -E -i \
    -e "/<Directory \/var\/www\/>/,/<\/Directory>/ s/AllowOverride None/AllowOverride All/" \
    /etc/apache2/apache2.conf

# Backup default web and use our code.
sudo mv /var/www/html /var/www/html.orig
sudo ln -s /opt/web /var/www/html

# Restart apache since we added mod_rewrite.
sudo service apache2 restart
